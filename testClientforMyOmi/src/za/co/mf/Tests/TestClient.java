package za.co.mf.Tests;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.swagger.client.ApiClient;
import io.swagger.client.api.DefaultApi;

public class TestClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TestClient tClient = new TestClient();
		tClient.testDataEngine();
	}
		
	
	
	public void testDataEngine() {
		//call rulesEngine to check what basis values are applicable
        za.co.omi.dataEngine.DataEngine dataEngine = new za.co.omi.dataEngine.DataEngine();
        HashMap returnValues = new HashMap();
        
        String basePath = "http://mf063146:8080/retail/data/engine/v1";
        String countrycode = "ZA";
        String policytype = "CL";
        String transactiontype = "Q";
        String productid = "09";
        String sectionid = "0071";
        String itemid = "168";
        String tablename = "BASIS";
        String version = "4";
        String language = "en";
        String validationfield = "POLICY_DATE";
        String validationvalue = "2021-08-02";
        
        DefaultApi defaultApi = new DefaultApi();
        
        //DefaultApi api = new DefaultApi(new ApiClient());
        //String code = "";
        //AccessToken response = api.authorize(code, redirectUri);
        //assertNotNull(response);
        //assertNotNull(response.getAccess_token());
        //assertEquals("bearer", response.getToken_type());
        
        
        
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("http://mf063146:8080/retail/data/engine/v1");
        apiClient.addDefaultHeader("Authorization", "bearer TOKEN");
        /*OkHttpClient httpClient = apiClient.getHttpClient();
        httpClient.setConnectTimeout(60, TimeUnit.SECONDS);
        httpClient.setReadTimeout(60, TimeUnit.SECONDS);
        httpClient.setWriteTimeout(60, TimeUnit.SECONDS);*/
        //defaultApi.setApiClient(apiClient);

        //SomeModel var1 = defaultApi.getNodesTest();
        
        
        
        returnValues = dataEngine.getData(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        // Getting an iterator
        Iterator hmIterator = returnValues.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            String description = ((String)mapElement.getValue());
            System.out.println(mapElement.getKey() + " : " + description);
        }
        
       //data.getXlaPeRefBasis().setData(returnValues);
	}

}
