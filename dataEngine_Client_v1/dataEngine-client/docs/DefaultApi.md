# DefaultApi

All URIs are relative to *https://localhost:8080/retail/data/engine/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDataItems**](DefaultApi.md#getDataItems) | **GET** /countries/countrycodes/{countrycode}/policies/policytypes/{policytype}/transactions/transactiontypes/{transactiontype}/products/productids/{productid}/sections/sectionids/{sectionid}/items/itemids/{itemid} | 


<a name="getDataItems"></a>
# **getDataItems**
> DataItemsResponse getDataItems(countrycode, policytype, transactiontype, productid, sectionid, itemid, tablename, code, language, validationfield, validationvalue)



### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String countrycode = "countrycode_example"; // String | 
String policytype = "policytype_example"; // String | 
String transactiontype = "transactiontype_example"; // String | 
String productid = "productid_example"; // String | 
String sectionid = "sectionid_example"; // String | 
String itemid = "itemid_example"; // String | 
String tablename = "tablename_example"; // String | 
String code = "code_example"; // String | 
String language = "language_example"; // String | 
String validationfield = "validationfield_example"; // String | 
String validationvalue = "validationvalue_example"; // String | 
try {
    DataItemsResponse result = apiInstance.getDataItems(countrycode, policytype, transactiontype, productid, sectionid, itemid, tablename, code, language, validationfield, validationvalue);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getDataItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **countrycode** | **String**|  |
 **policytype** | **String**|  |
 **transactiontype** | **String**|  |
 **productid** | **String**|  |
 **sectionid** | **String**|  |
 **itemid** | **String**|  |
 **tablename** | **String**|  |
 **code** | **String**|  |
 **language** | **String**|  |
 **validationfield** | **String**|  |
 **validationvalue** | **String**|  |

### Return type

[**DataItemsResponse**](DataItemsResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

