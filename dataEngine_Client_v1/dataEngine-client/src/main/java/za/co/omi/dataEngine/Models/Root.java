package za.co.omi.dataEngine.Models;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Root {
    @JsonProperty("code")
    public String code;
    @JsonProperty("value")
    public String value;
}