package za.co.omi.dataEngine;

//import io.swagger.client.api.DefaultApiTest;

import io.swagger.client.api.DefaultApiTest;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class DataEngine {


    public static void main(String[] args) {
        DataEngine start = new DataEngine();
        HashMap returnValues = new HashMap();

        String basePath = "http://mf063146:8080/retail/data/engine/v1";
        String countrycode = "ZA";
        String policytype = "CL";
        String transactiontype = "Q";
        String productid = "09";
        String sectionid = "0071";
        String itemid = "168";
        String tablename = "BASIS";
        String version = "4";
        String language = "en";
        String validationfield = "POLICY_DATE";
        String validationvalue = "2021-07-31";

        System.out.println("test policy date 2021-07-31");
        returnValues = start.getData(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        // Getting an iterator
        Iterator hmIterator = returnValues.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            String description = ((String)mapElement.getValue());
            System.out.println(mapElement.getKey() + " : " + description);
        }

        countrycode = "ZA";
        policytype = "CL";
        transactiontype = "Q";
        productid = "09";
        sectionid = "0071";
        itemid = "168";
        tablename = "BASIS";
        version = "4";
        language = "en";
        validationfield = "POLICY_DATE";
        validationvalue = "2021-08-20";
        System.out.println("test policy date 2021-08-20");
        returnValues = start.getData(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        // Getting an iterator
        hmIterator = returnValues.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            String description = ((String)mapElement.getValue());
            System.out.println(mapElement.getKey() + " : " + description);
        }

        countrycode = "ZA";
        policytype = "CL";
        transactiontype = "Q";
        productid = "09";
        sectionid = "0071";
        itemid = "168";
        tablename = "BASIS";
        version = "4";
        language = "en";
        validationfield = "RETAIL_VALUE";
        validationvalue = "10000";
        System.out.println("test quote retail value 10000");
        returnValues = start.getData(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        // Getting an iterator
        hmIterator = returnValues.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            String description = ((String)mapElement.getValue());
            System.out.println(mapElement.getKey() + " : " + description);
        }

        countrycode = "ZA";
        policytype = "CL";
        transactiontype = "Q";
        productid = "09";
        sectionid = "0071";
        itemid = "168";
        tablename = "BASIS";
        version = "4";
        language = "en";
        validationfield = "RETAIL_VALUE";
        validationvalue = "9999";
        System.out.println("test retail value 9999");
        returnValues = start.getData(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        // Getting an iterator
        hmIterator = returnValues.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            String description = ((String)mapElement.getValue());
            System.out.println(mapElement.getKey() + " : " + description);
        }

        countrycode = "ZA";
        policytype = "CL";
        transactiontype = "M";
        productid = "09";
        sectionid = "0071";
        itemid = "168";
        tablename = "BASIS";
        version = "4";
        language = "en";
        validationfield = "BASIS";
        validationvalue = "3";
        System.out.println("test MTA policy with basis 3");
        returnValues = start.getData(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        // Getting an iterator
        hmIterator = returnValues.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            String description = ((String)mapElement.getValue());
            System.out.println(mapElement.getKey() + " : " + description);
        }


        countrycode = "ZA";
        policytype = "CL";
        transactiontype = "M";
        productid = "09";
        sectionid = "0071";
        itemid = "168";
        tablename = "BASIS";
        version = "4";
        language = "en";
        validationfield = "BASIS";
        validationvalue = "1";
        System.out.println("test mta policy with basis 1");
        returnValues = start.getData(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        // Getting an iterator
        hmIterator = returnValues.entrySet().iterator();
        while (hmIterator.hasNext()) {
            Map.Entry mapElement = (Map.Entry)hmIterator.next();
            String description = ((String)mapElement.getValue());
            System.out.println(mapElement.getKey() + " : " + description);
        }

    }


    public HashMap getData(String basePath , String countrycode,String policytype,String transactiontype,String productid,String sectionid,String itemid,String tablename, String version,
                                        String language,String validationfield, String validationvalue) {



        DefaultApiTest apiTest = new DefaultApiTest();
        HashMap returnValues = new HashMap();
        long startTime = new Date().getTime();
        try {
            returnValues = apiTest.getDataItems(basePath,countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version , language,validationfield,validationvalue);
        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            System.out.println("Time: " + (new Date().getTime() - startTime) + "ms");
            return returnValues;
        }



    }


}
