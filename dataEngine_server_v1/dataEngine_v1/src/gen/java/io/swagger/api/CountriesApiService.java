package io.swagger.api;

import io.swagger.api.*;
import io.swagger.model.*;


import io.swagger.model.DataItemsResponse;
import io.swagger.model.ResultSet;

import java.util.List;
import io.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2021-07-27T13:23:48.588Z")
public interface CountriesApiService {
      Response getDataItems(String countrycode,String policytype,String transactiontype,String productid,String sectionid,String itemid,String tablename,String code,String language,String validationfield,String validationvalue,SecurityContext securityContext)
      throws NotFoundException;
}
