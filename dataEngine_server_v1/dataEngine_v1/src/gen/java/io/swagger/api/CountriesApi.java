package io.swagger.api;

import io.swagger.model.*;
import io.swagger.api.CountriesApiService;

import io.swagger.annotations.ApiParam;
import io.swagger.jaxrs.*;

import io.swagger.model.DataItemsResponse;
import io.swagger.model.ResultSet;

import java.util.Map;
import java.util.List;
import io.swagger.api.NotFoundException;

import java.io.InputStream;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.*;
import javax.inject.Inject;

import javax.validation.constraints.*;

@Path("/countries")
@Consumes({ "application/json" })
@Produces({ "application/json" })
@io.swagger.annotations.Api(description = "the countries API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2021-07-27T13:23:48.588Z")
public class CountriesApi  {

    @Inject CountriesApiService service;

    @GET
    @Path("/countrycodes/{countrycode}/policies/policytypes/{policytype}/transactions/transactiontypes/{transactiontype}/products/productids/{productid}/sections/sectionids/{sectionid}/items/itemids/{itemid}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @io.swagger.annotations.ApiOperation(value = "", notes = "", response = DataItemsResponse.class, tags={  })
    @io.swagger.annotations.ApiResponses(value = { 
        @io.swagger.annotations.ApiResponse(code = 200, message = "normal response", response = DataItemsResponse.class),
        
        @io.swagger.annotations.ApiResponse(code = 500, message = "Internal Server Error", response = ResultSet.class) })
    public Response getDataItems( @PathParam("countrycode") String countrycode, @PathParam("policytype") String policytype, @PathParam("transactiontype") String transactiontype, @PathParam("productid") String productid, @PathParam("sectionid") String sectionid, @PathParam("itemid") String itemid, @NotNull  @QueryParam("tablename") String tablename, @NotNull  @QueryParam("version") String version, @NotNull  @QueryParam("language") String language, @NotNull  @QueryParam("validationfield") String validationfield, @NotNull  @QueryParam("validationvalue") String validationvalue,@Context SecurityContext securityContext)
    throws NotFoundException {
        return service.getDataItems(countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version,language,validationfield,validationvalue,securityContext);
    }
}
