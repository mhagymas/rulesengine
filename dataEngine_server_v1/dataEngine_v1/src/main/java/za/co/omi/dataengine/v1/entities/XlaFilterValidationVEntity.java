package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "XLA_FILTER_VALIDATION_V", schema = "TIA", catalog = "")
@NamedQueries({
        @NamedQuery(name = "VALIDATION.RULE", query = "select a from XlaFilterValidationVEntity a,DatatypeVEntity b, XlaFilterVEntity c , XlaFilterValueVEntity d where a.datatypeVByDatatypeId.datatypeId =  b.datatypeId and a.xlaFilterVByXlaFilterId.xlaFilterId = c.xlaFilterId and c.countryVByCountryCode.countryCode = :countrycode and c.policyInfoVByPolicyInfoId.policyInfoId = :policyInfoId and c.transactionTypeVByTransactionTypeId.transactionTypeId = :transactionTypeId and c.productVByProductId.productId = :productId and c.sectionVBySectionId.sectionId = :sectionId and c.itemVByItemId.itemId = :itemId and a.xlaFilterValueVByXlaFilterValueId.xlaFilterValueId = d.xlaFilterValueId and d.tableName = :tableName and d.version = :version and d.language = :language and a.validationField = :validationField")})
/* Reference below for NameQuery
select A.VALIDATION_FIELD, A.VALIDATION_OPERATOR, A.VALIDATION_VALUE, B.DATATYPE_NAME, A.CODES
        FROM
        TIA.XLA_FILTER_VALIDATION_V A,
        TIA.DATATYPE_V B,
        TIA.XLA_FILTER_V C,
        TIA.XLA_FILTER_VALUE_V D
        WHERE
        A.DATATYPE_ID = B.DATATYPE_ID
        AND
        A.XLA_FILTER_ID = C.XLA_FILTER_ID
        AND C.COUNTRY_CODE = 'ZA' AND C.POLICY_INFO_ID = 'CL' AND C.TRANSACTION_TYPE_ID = 'Q' AND C.PRODUCT_ID = '09' AND C.SECTION_ID = '0071' AND C.ITEM_ID = '168'
        AND
        A.XLA_FILTER_VALUE_ID = D.XLA_FILTER_VALUE_ID
        AND
        D.TABLE_NAME = 'BASIS' AND D.VERSION = '4' AND D.LANGUAGE = 'en'
        AND A.VALIDATION_FIELD = 'POLICY_DATE';
*/







public class XlaFilterValidationVEntity {
    private long xlaFilterValidationId;
    private String validationField;
    private String validationOperator;
    private String validationValue;
    private String codes;
    private DatatypeVEntity datatypeVByDatatypeId;
    private XlaFilterVEntity xlaFilterVByXlaFilterId;
    private XlaFilterValueVEntity xlaFilterValueVByXlaFilterValueId;

    @Id
    @Column(name = "XLA_FILTER_VALIDATION_ID")
    public long getXlaFilterValidationId() {
        return xlaFilterValidationId;
    }

    public void setXlaFilterValidationId(long xlaFilterValidationId) {
        this.xlaFilterValidationId = xlaFilterValidationId;
    }

    @Basic
    @Column(name = "VALIDATION_FIELD")
    public String getValidationField() {
        return validationField;
    }

    public void setValidationField(String validationField) {
        this.validationField = validationField;
    }

    @Basic
    @Column(name = "VALIDATION_OPERATOR")
    public String getValidationOperator() {
        return validationOperator;
    }

    public void setValidationOperator(String validationOperator) {
        this.validationOperator = validationOperator;
    }

    @Basic
    @Column(name = "VALIDATION_VALUE")
    public String getValidationValue() {
        return validationValue;
    }

    public void setValidationValue(String validationValue) {
        this.validationValue = validationValue;
    }

    @Basic
    @Column(name = "CODES")
    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XlaFilterValidationVEntity that = (XlaFilterValidationVEntity) o;

        if (xlaFilterValidationId != that.xlaFilterValidationId) return false;
        if (validationField != null ? !validationField.equals(that.validationField) : that.validationField != null)
            return false;
        if (validationOperator != null ? !validationOperator.equals(that.validationOperator) : that.validationOperator != null)
            return false;
        if (validationValue != null ? !validationValue.equals(that.validationValue) : that.validationValue != null)
            return false;
        if (codes != null ? !codes.equals(that.codes) : that.codes != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (xlaFilterValidationId ^ (xlaFilterValidationId >>> 32));
        result = 31 * result + (validationField != null ? validationField.hashCode() : 0);
        result = 31 * result + (validationOperator != null ? validationOperator.hashCode() : 0);
        result = 31 * result + (validationValue != null ? validationValue.hashCode() : 0);
        result = 31 * result + (codes != null ? codes.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "DATATYPE_ID", referencedColumnName = "DATATYPE_ID", nullable = false , insertable = false, updatable = false)
    public DatatypeVEntity getDatatypeVByDatatypeId() {
        return datatypeVByDatatypeId;
    }

    public void setDatatypeVByDatatypeId(DatatypeVEntity datatypeVByDatatypeId) {
        this.datatypeVByDatatypeId = datatypeVByDatatypeId;
    }

    @ManyToOne
    @JoinColumn(name = "XLA_FILTER_ID", referencedColumnName = "XLA_FILTER_ID", nullable = false, insertable = false, updatable = false)
    public XlaFilterVEntity getXlaFilterVByXlaFilterId() {
        return xlaFilterVByXlaFilterId;
    }

    public void setXlaFilterVByXlaFilterId(XlaFilterVEntity xlaFilterVByXlaFilterId) {
        this.xlaFilterVByXlaFilterId = xlaFilterVByXlaFilterId;
    }

    @ManyToOne
    @JoinColumn(name = "XLA_FILTER_VALUE_ID", referencedColumnName = "XLA_FILTER_VALUE_ID", nullable = false, insertable = false, updatable = false)
    public XlaFilterValueVEntity getXlaFilterValueVByXlaFilterValueId() {
        return xlaFilterValueVByXlaFilterValueId;
    }

    public void setXlaFilterValueVByXlaFilterValueId(XlaFilterValueVEntity xlaFilterValueVByXlaFilterValueId) {
        this.xlaFilterValueVByXlaFilterValueId = xlaFilterValueVByXlaFilterValueId;
    }
}
