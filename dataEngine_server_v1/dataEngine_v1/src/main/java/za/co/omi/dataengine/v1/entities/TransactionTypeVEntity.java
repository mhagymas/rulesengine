package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "TRANSACTION_TYPE_V", schema = "TIA", catalog = "")
public class TransactionTypeVEntity {
    private String transactionTypeId;
    private String transactionTypeName;

    @Id
    @Column(name = "TRANSACTION_TYPE_ID")
    public String getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(String transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    @Basic
    @Column(name = "TRANSACTION_TYPE_NAME")
    public String getTransactionTypeName() {
        return transactionTypeName;
    }

    public void setTransactionTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionTypeVEntity that = (TransactionTypeVEntity) o;

        if (transactionTypeId != null ? !transactionTypeId.equals(that.transactionTypeId) : that.transactionTypeId != null)
            return false;
        if (transactionTypeName != null ? !transactionTypeName.equals(that.transactionTypeName) : that.transactionTypeName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = transactionTypeId != null ? transactionTypeId.hashCode() : 0;
        result = 31 * result + (transactionTypeName != null ? transactionTypeName.hashCode() : 0);
        return result;
    }
}
