package za.co.omi.dataengine.v1.utils;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {

    public boolean  isValidRule(String validationOperator , String validationValue , String dataTypeName , String validationInput) {
        boolean isValidRule = false;

        if (dataTypeName.equals("date")) {
            return validateDate(validationOperator ,validationValue , dataTypeName , validationInput);
        }

        if (dataTypeName.equals("string")) {
            return validateString(validationOperator ,validationValue , dataTypeName , validationInput);
        }

        if (dataTypeName.equals("double")) {
            return validateDouble(validationOperator ,validationValue , dataTypeName , validationInput);
        }

        return isValidRule;
    }

    public boolean validateString(String validationOperator , String validationValue , String dataTypeName , String validationInput) {
        boolean isValidString = false;
        try {
            if (validationOperator.equals("==")) {
                if (validationValue.equals(validationInput)) {
                    return true;
                }
            }

            if (validationOperator.equals("!=")) {
                if (!validationValue.equals(validationInput)) {
                    return true;
                }
            }

        } catch(Exception ex) {
            return false;
        }

        return isValidString;
    }

    public boolean validateDouble(String validationOperator , String validationValue , String dataTypeName , String validationInput) {
        boolean isValidDouble = false;


        try {
            double valInputDouble = Double.parseDouble(validationInput);
            double valValidationDouble = Double.parseDouble(validationValue);
            if(validationOperator.equals(">")) {
                if (valInputDouble > valValidationDouble) {
                    return true;
                }
            }
            if(validationOperator.equals("==")) {
                if (valInputDouble == valValidationDouble) {
                    return true;
                }
            }

            if(validationOperator.equals("<")) {
                if (valInputDouble < valValidationDouble) {
                    return true;
                }
            }

            if(validationOperator.equals("!=")) {
                if (valInputDouble != valValidationDouble) {
                    return true;
                }
            }

            if(validationOperator.equals(">=")) {
                if (valInputDouble >= valValidationDouble) {
                    return true;
                }
            }

            if(validationOperator.equals("<=")) {
                if (valInputDouble <= valValidationDouble) {
                    return true;
                }
            }
        } catch(Exception ex) {
            return false;
        }

        return isValidDouble;
    }


    public boolean validateDate(String validationOperator , String validationValue , String dataTypeName , String validationInput) {
        boolean isValidDate = false;

        try {
            Date valValidationDate=new SimpleDateFormat("yyyy-MM-dd").parse(validationValue);
            Date valInputDate=new SimpleDateFormat("yyyy-MM-dd").parse(validationInput);
            if(validationOperator.equals(">")) {
                if (valInputDate.after(valValidationDate)) {
                    return true;
                }
            }
            if(validationOperator.equals("==")) {
                if (valInputDate.equals(valValidationDate)) {
                    return true;
                }
            }

            if(validationOperator.equals("<")) {
                if (valInputDate.before(valValidationDate)) {
                    return true;
                }
            }

            if(validationOperator.equals("!=")) {
                if (!valInputDate.equals(valValidationDate)) {
                    return true;
                }
            }

            if(validationOperator.equals(">=")) {
                if (valInputDate.after(valValidationDate) || valInputDate.equals(valValidationDate) ) {
                    return true;
                }
            }

            if(validationOperator.equals("<=")) {
                if (valInputDate.before(valValidationDate) || valInputDate.equals(valValidationDate)) {
                    return true;
                }
            }
        }

        catch(Exception ex){
            return false;
        }


        return isValidDate;
    }

}
