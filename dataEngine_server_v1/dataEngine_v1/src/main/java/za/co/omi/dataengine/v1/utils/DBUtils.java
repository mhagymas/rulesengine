package za.co.omi.dataengine.v1.utils;


/*import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;*/

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

//import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
//import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class DBUtils {
    //DataSource dataSource = (DataSource) context.lookup("java:comp/env/jdbc/myDB");


    /*public MongoDatabase getMongoDB()
    {
        LoggerUtil loggerUtil = new LoggerUtil();
        try {
            Context context = new InitialContext();
            //mongodb://mongoadmin:secret@localhost:27017
            CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                    fromProviders(PojoCodecProvider.builder().automatic(true).build()));

            ConnectionString conn = new ConnectionString((String)context.lookup("java:global/env/url/mongo-myomi"));
            MongoClientSettings settings = MongoClientSettings.builder()
                    .codecRegistry(pojoCodecRegistry).applyConnectionString(conn)
                    .build();

            MongoClient mongo = MongoClients.create(settings);//((String)context.lookup("java:global/env/url/mongo-myomi"));
            MongoDatabase db = mongo.getDatabase("myomi");

            return db;
        }
        catch (Exception ex)
        {
            loggerUtil.logInfo("[DBUtils] - " + ex.getMessage());
            return null;
        }

    }*/

    public EntityManagerFactory getEntityManager()
    {
        LoggerUtil loggerUtil = new LoggerUtil();
        String url = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=zamfdodbv101-vip.mufed.co.za)(PORT=1400))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=tiadev.mufed.co.za)))";
        String driverClass = "oracle.jdbc.OracleDriver";
        String userName = "TIA";
        String password = "SC#2016#TIADEV";
        String showSQL = "true";
        String dialect = "org.hibernate.dialect.Oracle9iDialect";


        try {
            Context context = new InitialContext();
            url = (String)context.lookup("java:global/env/persistence/Url");
            driverClass = (String)context.lookup("java:global/env/persistence/driverClass");
            userName = (String)context.lookup("java:global/env/persistence/userName");
            password = (String)context.lookup("java:global/env/persistence/password");
            showSQL = (String)context.lookup("java:global/env/persistence/showSQL");
            dialect = (String)context.lookup("java:global/env/persistence/dailect");
            //mongodb://mongoadmin:secret@localhost:27017
            loggerUtil.logInfo("(url)  " + url + " (driverClass) " + driverClass + " (username) " + userName + " (password) " +  password + " (showSQL) " + showSQL + " (dialect) " + dialect);

            Map<String, String> persistenceMap = new HashMap<String, String>();

            persistenceMap.put("hibernate.connection.url",url);
            persistenceMap.put("hibernate.connection.driver_class" ,driverClass);
            persistenceMap.put("hibernate.connection.user" ,userName);
            persistenceMap.put("hibernate.connection.password" ,password);
            persistenceMap.put("hibernate.show_sql" ,showSQL);
            persistenceMap.put("hibernate.dialect" ,dialect);



            //create reference to persistence unit name for db
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default",persistenceMap);

            return entityManagerFactory;

        }
        catch (Exception ex)
        {
            loggerUtil.logInfo("[DBUtils] - " + ex.getMessage());
            return null;
        }

    }


    public java.sql.Date ToDBDate(java.util.Date dt)
    {
        return new Date(dt.getTime());
    }



}
