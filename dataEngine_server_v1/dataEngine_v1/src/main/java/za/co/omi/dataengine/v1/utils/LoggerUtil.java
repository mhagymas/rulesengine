package za.co.omi.dataengine.v1.utils;


import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class LoggerUtil {
    Logger logger = LogManager.getLogger(LoggerUtil.class);

    public LoggerUtil()
    {
        PropertyConfigurator.configure("/log4j/log4j.properties");
    }

    public void logInfo(String message)
    {
        logger.log(Level.INFO,message);
    }

    public void logError(String message)
    {
        logger.log(Level.ERROR, message);
    }

    public void logDebug(String message)
    {
        logger.log(Level.DEBUG,message);
    }
}
