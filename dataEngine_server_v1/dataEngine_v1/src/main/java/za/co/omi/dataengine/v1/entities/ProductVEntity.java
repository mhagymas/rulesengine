package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_V", schema = "TIA", catalog = "")
public class ProductVEntity {
    private String productId;
    private String productName;
    private PolicyInfoVEntity policyInfoVByPolicyInfoId;

    @Id
    @Column(name = "PRODUCT_ID")
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "PRODUCT_NAME")
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductVEntity that = (ProductVEntity) o;

        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productId != null ? productId.hashCode() : 0;
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "POLICY_INFO_ID", referencedColumnName = "POLICY_INFO_ID", nullable = false, insertable = false, updatable = false)
    public PolicyInfoVEntity getPolicyInfoVByPolicyInfoId() {
        return policyInfoVByPolicyInfoId;
    }

    public void setPolicyInfoVByPolicyInfoId(PolicyInfoVEntity policyInfoVByPolicyInfoId) {
        this.policyInfoVByPolicyInfoId = policyInfoVByPolicyInfoId;
    }
}
