package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "XLA_FILTER_VALUE_V", schema = "TIA", catalog = "")
public class XlaFilterValueVEntity {
    private long xlaFilterValueId;
    private String tableName;
    private String version;
    private String language;

    @Id
    @Column(name = "XLA_FILTER_VALUE_ID")
    public long getXlaFilterValueId() {
        return xlaFilterValueId;
    }

    public void setXlaFilterValueId(long xlaFilterValueId) {
        this.xlaFilterValueId = xlaFilterValueId;
    }

    @Basic
    @Column(name = "TABLE_NAME")
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Basic
    @Column(name = "VERSION")
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "LANGUAGE")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XlaFilterValueVEntity that = (XlaFilterValueVEntity) o;

        if (xlaFilterValueId != that.xlaFilterValueId) return false;
        if (tableName != null ? !tableName.equals(that.tableName) : that.tableName != null) return false;
        if (version != null ? !version.equals(that.version) : that.version != null) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (xlaFilterValueId ^ (xlaFilterValueId >>> 32));
        result = 31 * result + (tableName != null ? tableName.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        return result;
    }
}
