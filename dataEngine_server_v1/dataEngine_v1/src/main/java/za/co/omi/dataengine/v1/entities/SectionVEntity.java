package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "SECTION_V", schema = "TIA", catalog = "")
public class SectionVEntity {
    private String sectionId;
    private String sectionName;

    @Id
    @Column(name = "SECTION_ID", nullable = false, length = 19)
    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    @Basic
    @Column(name = "SECTION_NAME", nullable = false, length = 100)
    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SectionVEntity that = (SectionVEntity) o;

        if (sectionId != null ? !sectionId.equals(that.sectionId) : that.sectionId != null) return false;
        if (sectionName != null ? !sectionName.equals(that.sectionName) : that.sectionName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sectionId != null ? sectionId.hashCode() : 0;
        result = 31 * result + (sectionName != null ? sectionName.hashCode() : 0);
        return result;
    }
}
