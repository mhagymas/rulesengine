package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "DATATYPE_V", schema = "TIA", catalog = "")
public class DatatypeVEntity {
    private long datatypeId;
    private String datatypeName;

    @Id
    @Column(name = "DATATYPE_ID")
    public long getDatatypeId() {
        return datatypeId;
    }

    public void setDatatypeId(long datatypeId) {
        this.datatypeId = datatypeId;
    }

    @Basic
    @Column(name = "DATATYPE_NAME")
    public String getDatatypeName() {
        return datatypeName;
    }

    public void setDatatypeName(String datatypeName) {
        this.datatypeName = datatypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DatatypeVEntity that = (DatatypeVEntity) o;

        if (datatypeId != that.datatypeId) return false;
        if (datatypeName != null ? !datatypeName.equals(that.datatypeName) : that.datatypeName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (datatypeId ^ (datatypeId >>> 32));
        result = 31 * result + (datatypeName != null ? datatypeName.hashCode() : 0);
        return result;
    }
}
