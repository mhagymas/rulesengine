package za.co.omi.dataengine.v1.server;

import io.swagger.api.*;
import io.swagger.model.*;


import io.swagger.model.DataItemsResponse;
import io.swagger.model.ResultSet;

import java.util.*;

import io.swagger.api.NotFoundException;
import za.co.omi.dataengine.v1.entities.XlaFilterValidationVEntity;
import za.co.omi.dataengine.v1.entities.XlaPeReferenceEntity;
import za.co.omi.dataengine.v1.utils.DBUtils;
import za.co.omi.dataengine.v1.utils.LoggerUtil;
import za.co.omi.dataengine.v1.utils.Utilities;

import java.io.InputStream;

import javax.enterprise.context.RequestScoped;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.*;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@RequestScoped
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2021-07-27T13:23:48.588Z")
public class CountriesApiServiceImpl implements CountriesApiService {
    public Response getDataItems(String countrycode, String policytype, String transactiontype, String productid, String sectionid, String itemid, String tablename, String version, String language, String validationfield, String validationvalue, SecurityContext securityContext)
            throws NotFoundException {
        // do some magic!
        DBUtils dbUtils = new DBUtils();

        DataItemsResponse response = new DataItemsResponse();
        LoggerUtil loggerUtil = new LoggerUtil();
        loggerUtil.logInfo("Start field validations");
        ResultSet resultSet = new ResultSet();
        boolean validData = false;
        //--------- start field validations
        Validations val = new Validations();
        ArrayList<String> messages = val.doValidations(countrycode, policytype, transactiontype, productid, sectionid, itemid, tablename, version,language,validationfield, validationvalue);
        if (messages.size() > 0 ) {
            resultSet.setResultCode("R01");
            resultSet.setResultDescription(messages.toString());
            //--------- end field validations
        }


        //--------- start jpa entity



        //create reference to persistence unit name for db
        EntityManagerFactory entityManagerFactory = dbUtils.getEntityManager();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            loggerUtil.logInfo("Find the rule for XlaFilterValidationVEntity");
            List<XlaFilterValidationVEntity> xlaFilterValidationVEntities = queryXlaFilterValidationVEntity(entityManager , countrycode,policytype,transactiontype,productid,sectionid,itemid,tablename,version,language,validationfield);

            Utilities utils = new Utilities();
            loggerUtil.logInfo("size of list : " + xlaFilterValidationVEntities.size());

            if (xlaFilterValidationVEntities.size() > 0) {
                loggerUtil.logInfo("rules found");
                for (XlaFilterValidationVEntity c : xlaFilterValidationVEntities) {
                    //entityManager.remove(c); -- this is for delete

                    loggerUtil.logInfo("chekc if rule is valid  : c.getValidationOperator() " + c.getValidationOperator() + " getValidationValue()  " + c.getValidationValue() + " getDatatypeVByDatatypeId().getDatatypeName " + c.getDatatypeVByDatatypeId().getDatatypeName() + " validationvalue " + validationvalue);
                    //check if the rule is valid

                    if (utils.isValidRule(c.getValidationOperator(), c.getValidationValue(), c.getDatatypeVByDatatypeId().getDatatypeName(), validationvalue)) {

                        loggerUtil.logInfo("Find descriptions for the code : tablename " + tablename + " language " + language + " version " + version + " codes " + c.getCodes());
                        List<XlaPeReferenceEntity> xlaPeReferenceEntities = queryXlaPeReferenceEntityByRule(entityManager,tablename,language,version , c.getCodes());

                        loggerUtil.logInfo("results found on pe table : " + xlaPeReferenceEntities.size());
                        if (xlaPeReferenceEntities.size() > 0) {
                            for (XlaPeReferenceEntity xlaPe : xlaPeReferenceEntities) {

                                DataItem dataItem = new DataItem();
                                loggerUtil.logInfo("code  : " + xlaPe.getCode() + " Decription : " + xlaPe.getDescription());
                                dataItem.setCode(xlaPe.getCode());
                                dataItem.setValue(xlaPe.getDescription());
                                response.add(dataItem);
                                validData = true;
                            }
                        }
                    }
                }
            }

            //no rule found with the codes
            if (response.size() == 0 ) {
                loggerUtil.logInfo("no rule found with values response.size = " + response.size() );
                loggerUtil.logInfo("check xlape reference with  tableName " + tablename + " language " + language  + " version " + version);
                List<XlaPeReferenceEntity> xlaPeReferenceEntities = queryXlaPeReferenceEntityAll(entityManager,tablename,language,version);

                loggerUtil.logInfo("results found on pe table : " + xlaPeReferenceEntities.size());

                //was valid table name
                if (xlaPeReferenceEntities.size() > 0) {
                    loggerUtil.logInfo("valid table and result found ");
                    for (XlaPeReferenceEntity xlaPe : xlaPeReferenceEntities) {
                        DataItem dataItem = new DataItem();
                        loggerUtil.logInfo("code  : " + xlaPe.getCode() + " Decription : " + xlaPe.getDescription());
                        dataItem.setCode(xlaPe.getCode());
                        dataItem.setValue(xlaPe.getDescription());
                        response.add(dataItem);
                        validData = true;
                    }
                }
            }

            transaction.commit();
            if (validData) {
                return Response.ok().entity(response).build();
            } else { //not valid table name
                UUID uuid = UUID.randomUUID();
                String randomUUIDString = uuid.toString();
                resultSet.setResultCode("R01");
                resultSet.setResultCode("Data not found: " + randomUUIDString);
                return Response.serverError().entity(resultSet).build();
            }


        } catch (Exception ex) {

            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();
            loggerUtil.logError("R01 : A technical error has occured: " + randomUUIDString + " Exception " + ex.getMessage());

            resultSet.setResultCode("R01");
            resultSet.setResultCode("A technical error has occured: " + randomUUIDString);

            return Response.serverError().entity(resultSet).build();
            //log exception with GUID

        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }


        //return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
    }

    /**
     * do a named query called validation.rule to get a spesific rule
     * @param entityManager
     * @param countrycode
     * @param policytype
     * @param transactiontype
     * @param productid
     * @param sectionid
     * @param itemid
     * @param tablename
     * @param version
     * @param language
     * @param validationfield
     * @return
     */
    public List<XlaFilterValidationVEntity> queryXlaFilterValidationVEntity(EntityManager entityManager , String countrycode , String policytype , String transactiontype , String productid , String  sectionid , String  itemid , String tablename , String  version , String language  , String  validationfield) {

        LoggerUtil loggerUtil = new LoggerUtil();
        TypedQuery<XlaFilterValidationVEntity> xlaFilterValidationVEntityTypedQuery = entityManager.createNamedQuery("VALIDATION.RULE", XlaFilterValidationVEntity.class);
        loggerUtil.logInfo("set parameters : " + " countrycode: " + countrycode + " policyInfoId: " + policytype + " transactionTypeId: " + transactiontype + " productId: " + productid + " sectionId: " + sectionid + " itemId: " + itemid + " tableName: " + tablename + " version: " + version + " language: " + language + " validationField: " + validationfield);
        //set all parameters
        xlaFilterValidationVEntityTypedQuery.setParameter("countrycode", countrycode);
        xlaFilterValidationVEntityTypedQuery.setParameter("policyInfoId", policytype);
        xlaFilterValidationVEntityTypedQuery.setParameter("transactionTypeId", transactiontype);
        xlaFilterValidationVEntityTypedQuery.setParameter("productId", productid);
        xlaFilterValidationVEntityTypedQuery.setParameter("sectionId", sectionid);
        xlaFilterValidationVEntityTypedQuery.setParameter("itemId", itemid);
        xlaFilterValidationVEntityTypedQuery.setParameter("tableName", tablename);
        xlaFilterValidationVEntityTypedQuery.setParameter("version", version);
        xlaFilterValidationVEntityTypedQuery.setParameter("language", language);
        xlaFilterValidationVEntityTypedQuery.setParameter("validationField", validationfield);
        loggerUtil.logInfo("get result list");
        List<XlaFilterValidationVEntity> xlaFilterValidationVEntities = xlaFilterValidationVEntityTypedQuery.getResultList();
        return xlaFilterValidationVEntities;
    }

    /**
     * Do a named query called find rule to get all the values for a spesific rule
     * @param entityManager
     * @param tablename
     * @param language
     * @param version
     * @param codes
     * @return
     */
    public List<XlaPeReferenceEntity> queryXlaPeReferenceEntityByRule(EntityManager entityManager , String tablename , String language , String version , String codes ) {
        //Go get the descriptions on the pe reference table for the codes
        TypedQuery<XlaPeReferenceEntity> xlaPeReferenceEntityTypedQuery = entityManager.createNamedQuery("XlaPeReference.findbyRule", XlaPeReferenceEntity.class);
        xlaPeReferenceEntityTypedQuery.setParameter("tableName", tablename);
        xlaPeReferenceEntityTypedQuery.setParameter("langauge", language);
        long versionL = Long.valueOf(version);
        xlaPeReferenceEntityTypedQuery.setParameter("version", versionL);
        //because multiple codes inline
        String[] valCodes = codes.split(",");
        List<String> listCodes = Arrays.asList(valCodes);

        xlaPeReferenceEntityTypedQuery.setParameter("codes", listCodes);
        List<XlaPeReferenceEntity> xlaPeReferenceEntities = xlaPeReferenceEntityTypedQuery.getResultList();
        return xlaPeReferenceEntities;
    }

    /**
     * Do a call to xlaPeReference to get all the valid codes for a table name
     * @param entityManager
     * @param tablename
     * @param language
     * @param version
     * @return
     */
    public List<XlaPeReferenceEntity> queryXlaPeReferenceEntityAll(EntityManager entityManager , String tablename , String language , String version ) {
        //Go get the descriptions on the pe reference table for the codes
        TypedQuery<XlaPeReferenceEntity> xlaPeReferenceEntityTypedQuery = entityManager.createNamedQuery("XlaPeReference.findAll", XlaPeReferenceEntity.class);
        xlaPeReferenceEntityTypedQuery.setParameter("tableName", tablename);
        xlaPeReferenceEntityTypedQuery.setParameter("langauge", language);
        long versionL = Long.valueOf(version);
        xlaPeReferenceEntityTypedQuery.setParameter("version", versionL);
        List<XlaPeReferenceEntity> xlaPeReferenceEntities = xlaPeReferenceEntityTypedQuery.getResultList();
        return xlaPeReferenceEntities;
    }

}
