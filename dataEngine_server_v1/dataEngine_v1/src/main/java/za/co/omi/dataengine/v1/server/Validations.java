package za.co.omi.dataengine.v1.server;

import io.swagger.model.ResultSet;
import org.checkerframework.checker.units.qual.A;

import javax.ws.rs.core.Response;
import java.util.ArrayList;

public class Validations {
    public ArrayList<String> doValidations(String countrycode, String policytype, String transactiontype, String productid, String sectionid, String itemid, String tablename, String version, String language, String validationfield, String validationvalue) {
        ArrayList <String> messages = new ArrayList<>();

        if (countrycode == null) {
            messages.add("countrycode can not be blank");
        }


        if (policytype == null) {
            messages.add("policytype can not be blank");
        }

        if (transactiontype == null) {
            messages.add("transactiontype can not be blank");
        }


        if (productid == null) {
            messages.add("productid can not be blank");
        }


        if (countrycode == null) {
            messages.add("countrycode can not be blank");
        }


        if (itemid == null) {
            messages.add("itemid can not be blank");
        }


        if (tablename == null) {
            messages.add("tablename can not be blank");
        }


        if (version == null) {
            messages.add("code can not be blank");
        }


        if (language == null) {
            messages.add("language can not be blank");
        }


        if (validationfield == null) {
            messages.add("validationfield can not be blank");
        }


        if (validationvalue == null) {
            messages.add("validationvalue can not be blank");
        }

        return messages;
    };
}
