package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name = "XLA_PE_REFERENCE", schema = "TIA", catalog = "")
@NamedQueries({
        @NamedQuery(name = "XlaPeReference.findbyRule", query = "SELECT a from XlaPeReferenceEntity a where a.tableName = :tableName and  a.language = :langauge and a.version = :version and a.code in (:codes)") ,
        @NamedQuery(name = "XlaPeReference.findAll", query = "SELECT a from XlaPeReferenceEntity a where a.tableName = :tableName and  a.language = :langauge and a.version = :version")
})

@IdClass(XlaPeReferenceEntityPK.class)
public class XlaPeReferenceEntity {
    private String id;
    private String tableName;
    private long version;
    private String language;
    private String code;
    private String displayCode;
    private String description;
    private Time timestamp;
    private String userid;
    private Byte sortNo;
    private String helpText;
    private Long recordVersion;
    private String recordUserid;
    private Time recordTimestamp;
    private long uixSeqNo;

    @Id
    @Column(name = "ID")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Id
    @Column(name = "TABLE_NAME")
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Id
    @Column(name = "VERSION")
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Id
    @Column(name = "LANGUAGE")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Id
    @Column(name = "CODE")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "DISPLAY_CODE")
    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "TIMESTAMP")
    public Time getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Time timestamp) {
        this.timestamp = timestamp;
    }

    @Basic
    @Column(name = "USERID")
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "SORT_NO")
    public Byte getSortNo() {
        return sortNo;
    }

    public void setSortNo(Byte sortNo) {
        this.sortNo = sortNo;
    }

    @Basic
    @Column(name = "HELP_TEXT")
    public String getHelpText() {
        return helpText;
    }

    public void setHelpText(String helpText) {
        this.helpText = helpText;
    }

    @Basic
    @Column(name = "RECORD_VERSION")
    public Long getRecordVersion() {
        return recordVersion;
    }

    public void setRecordVersion(Long recordVersion) {
        this.recordVersion = recordVersion;
    }

    @Basic
    @Column(name = "RECORD_USERID")
    public String getRecordUserid() {
        return recordUserid;
    }

    public void setRecordUserid(String recordUserid) {
        this.recordUserid = recordUserid;
    }

    @Basic
    @Column(name = "RECORD_TIMESTAMP")
    public Time getRecordTimestamp() {
        return recordTimestamp;
    }

    public void setRecordTimestamp(Time recordTimestamp) {
        this.recordTimestamp = recordTimestamp;
    }

    @Basic
    @Column(name = "UIX_SEQ_NO")
    public long getUixSeqNo() {
        return uixSeqNo;
    }

    public void setUixSeqNo(long uixSeqNo) {
        this.uixSeqNo = uixSeqNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XlaPeReferenceEntity that = (XlaPeReferenceEntity) o;

        if (version != that.version) return false;
        if (uixSeqNo != that.uixSeqNo) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (tableName != null ? !tableName.equals(that.tableName) : that.tableName != null) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (displayCode != null ? !displayCode.equals(that.displayCode) : that.displayCode != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (timestamp != null ? !timestamp.equals(that.timestamp) : that.timestamp != null) return false;
        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;
        if (sortNo != null ? !sortNo.equals(that.sortNo) : that.sortNo != null) return false;
        if (helpText != null ? !helpText.equals(that.helpText) : that.helpText != null) return false;
        if (recordVersion != null ? !recordVersion.equals(that.recordVersion) : that.recordVersion != null)
            return false;
        if (recordUserid != null ? !recordUserid.equals(that.recordUserid) : that.recordUserid != null) return false;
        if (recordTimestamp != null ? !recordTimestamp.equals(that.recordTimestamp) : that.recordTimestamp != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (tableName != null ? tableName.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (displayCode != null ? displayCode.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (userid != null ? userid.hashCode() : 0);
        result = 31 * result + (sortNo != null ? sortNo.hashCode() : 0);
        result = 31 * result + (helpText != null ? helpText.hashCode() : 0);
        result = 31 * result + (recordVersion != null ? recordVersion.hashCode() : 0);
        result = 31 * result + (recordUserid != null ? recordUserid.hashCode() : 0);
        result = 31 * result + (recordTimestamp != null ? recordTimestamp.hashCode() : 0);
        result = 31 * result + (int) (uixSeqNo ^ (uixSeqNo >>> 32));
        return result;
    }
}
