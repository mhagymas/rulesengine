package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "POLICY_INFO_V", schema = "TIA", catalog = "")
public class PolicyInfoVEntity {
    private String policyInfoId;
    private String policyInfoType;
    private CountryVEntity countryVByCountryCode;

    @Id
    @Column(name = "POLICY_INFO_ID")
    public String getPolicyInfoId() {
        return policyInfoId;
    }

    public void setPolicyInfoId(String policyInfoId) {
        this.policyInfoId = policyInfoId;
    }

    @Basic
    @Column(name = "POLICY_INFO_TYPE")
    public String getPolicyInfoType() {
        return policyInfoType;
    }

    public void setPolicyInfoType(String policyInfoType) {
        this.policyInfoType = policyInfoType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PolicyInfoVEntity that = (PolicyInfoVEntity) o;

        if (policyInfoId != null ? !policyInfoId.equals(that.policyInfoId) : that.policyInfoId != null) return false;
        if (policyInfoType != null ? !policyInfoType.equals(that.policyInfoType) : that.policyInfoType != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = policyInfoId != null ? policyInfoId.hashCode() : 0;
        result = 31 * result + (policyInfoType != null ? policyInfoType.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "COUNTRY_CODE", referencedColumnName = "COUNTRY_CODE", nullable = false, insertable = false, updatable = false)
    public CountryVEntity getCountryVByCountryCode() {
        return countryVByCountryCode;
    }

    public void setCountryVByCountryCode(CountryVEntity countryVByCountryCode) {
        this.countryVByCountryCode = countryVByCountryCode;
    }
}
