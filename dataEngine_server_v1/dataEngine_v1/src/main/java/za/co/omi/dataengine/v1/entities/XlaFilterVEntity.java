package za.co.omi.dataengine.v1.entities;

import javax.persistence.*;

@Entity
@Table(name = "XLA_FILTER_V", schema = "TIA", catalog = "")
public class XlaFilterVEntity {
    private long xlaFilterId;
    private CountryVEntity countryVByCountryCode;
    private PolicyInfoVEntity policyInfoVByPolicyInfoId;
    private TransactionTypeVEntity transactionTypeVByTransactionTypeId;
    private ProductVEntity productVByProductId;
    private SectionVEntity sectionVBySectionId;
    private ItemVEntity itemVByItemId;

    @Id
    @Column(name = "XLA_FILTER_ID")
    public long getXlaFilterId() {
        return xlaFilterId;
    }

    public void setXlaFilterId(long xlaFilterId) {
        this.xlaFilterId = xlaFilterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XlaFilterVEntity that = (XlaFilterVEntity) o;

        if (xlaFilterId != that.xlaFilterId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (xlaFilterId ^ (xlaFilterId >>> 32));
    }

    @ManyToOne
    @JoinColumn(name = "COUNTRY_CODE", referencedColumnName = "COUNTRY_CODE", nullable = false, insertable = false, updatable = false)
    public CountryVEntity getCountryVByCountryCode() {
        return countryVByCountryCode;
    }

    public void setCountryVByCountryCode(CountryVEntity countryVByCountryCode) {
        this.countryVByCountryCode = countryVByCountryCode;
    }

    @ManyToOne
    @JoinColumn(name = "POLICY_INFO_ID", referencedColumnName = "POLICY_INFO_ID", nullable = false, insertable = false, updatable = false)
    public PolicyInfoVEntity getPolicyInfoVByPolicyInfoId() {
        return policyInfoVByPolicyInfoId;
    }

    public void setPolicyInfoVByPolicyInfoId(PolicyInfoVEntity policyInfoVByPolicyInfoId) {
        this.policyInfoVByPolicyInfoId = policyInfoVByPolicyInfoId;
    }

    @ManyToOne
    @JoinColumn(name = "TRANSACTION_TYPE_ID", referencedColumnName = "TRANSACTION_TYPE_ID", insertable = false, updatable = false)
    public TransactionTypeVEntity getTransactionTypeVByTransactionTypeId() {
        return transactionTypeVByTransactionTypeId;
    }

    public void setTransactionTypeVByTransactionTypeId(TransactionTypeVEntity transactionTypeVByTransactionTypeId) {
        this.transactionTypeVByTransactionTypeId = transactionTypeVByTransactionTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", insertable = false, updatable = false)
    public ProductVEntity getProductVByProductId() {
        return productVByProductId;
    }

    public void setProductVByProductId(ProductVEntity productVByProductId) {
        this.productVByProductId = productVByProductId;
    }

    @ManyToOne
    @JoinColumn(name = "SECTION_ID", referencedColumnName = "SECTION_ID", insertable = false, updatable = false)
    public SectionVEntity getSectionVBySectionId() {
        return sectionVBySectionId;
    }

    public void setSectionVBySectionId(SectionVEntity sectionVBySectionId) {
        this.sectionVBySectionId = sectionVBySectionId;
    }

    @ManyToOne
    @JoinColumn(name = "ITEM_ID", referencedColumnName = "ITEM_ID", insertable = false, updatable = false)
    public ItemVEntity getItemVByItemId() {
        return itemVByItemId;
    }

    public void setItemVByItemId(ItemVEntity itemVByItemId) {
        this.itemVByItemId = itemVByItemId;
    }
}
