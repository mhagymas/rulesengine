package za.co.omi.dataengine.v1.utils;

public class GlobalUtils {
    public boolean isValidMD5(String s) {
        return s.matches("^[a-fA-F0-9]{32}$");
    }
}
